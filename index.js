const express = require('express')

const app = express()

const port = 2000

app.use(express.json())
app.use(express.urlencoded({extended: true}))


let users = [{
	"username": "John Lloyd"
}]


//home page
app.get('/home', (req,res) => {
	res.send('Welcome to the Homepage!')
})


// list of users
app.get('/users', (request, response) => {
	response.send(users)
	console.log(users)
})

//delete
app.delete('/delete-user', (request, response) => {

    let message;
    let index;
    for (let i = 0; i < users.length; i++) {

        if (users[i].username == request.body.username) {
            index = i;
            message = `User ${users[i].username} has been deleted`;
                users.splice(index, 1);

            break;
        }
    }
    
    response.send(message);
    console.log(users);
});




app.listen(port, () => console.log(`Server is running at port ${port}`))